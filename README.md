## Instruction
1. Please copy the following quiestion in a Goodle doc and provide the answere of them.
2. Once you are done with them please share the link to the GDoc with `comment` access. Share the link of your GDoc with recruting manager.
3. Please create a public repository in Gitlab and push your code for the task into that repository. Make sure that you create a ReadME.md and cccopy the task description there.
4. Once your are done with your code and merged your code into the public repository, share the link of your repository with recruting manager.
## Questions:

1.  What DB size they worked (DB name/version, #rows in main table, what type of usual requests, what server size they used),  
    What issues faced (most hard/interesting),how solved it?

    Did you work with few millions record? describe how to handel it?

    Did you work with pre calculations? descibe it.

    Did you wotk with views materializations? describe it.

    Did you work with replication DB? describe it.

2.  Does they worked with queues, which, what issues faced, how solved it
3.  What PHP framework they have used, what issues faced (most interesting) how solved
4.  Does you work with race condition? How to solve it?

## Task
### Description of task
Please create structure of classes/interfaces that you will create for this module (realization is not needed):
Lets imagine, that you should create a plugin, working with files in project
Interface:
- put file (metadata, file content)
- get file metadata
- get file content
- remove file
  For the task  provide the followings:
    1. Models folder and files
    2. Service file: how to use models file
    3. good to have driver of how these files get inserted in the Database / S3 or any other storage service
