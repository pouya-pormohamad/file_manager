<?php

use FileManager\Exceptions\UnableToDeleteFile;
use FileManager\Exceptions\UnableToGetFile;
use FileManager\Exceptions\UnableToPutFile;
use FileManager\Exceptions\UnableToRetrieveMetadata;
use FileManager\Models\Adapter\AmazonS3Adapter;
use FileManager\Models\File\File;
use FileManager\Models\Driver\ImagineThisIsAmazonS3Drive;
use FileManager\Models\File\Metadata;

try {
    $amazonS3 = new ImagineThisIsAmazonS3Drive();

    $amazonS3Adapter = new AmazonS3Adapter($amazonS3);

    $fileManager = new File($amazonS3Adapter);

    $newMetadata = new Metadata([
        'mimeType'      => 'txt',
        'fileSize '     => 130,
        'lastModified ' => '12-24-2021 18:30:00'
    ]);

    $path = 'thisIsExampleFileName.txt';

    $fileManager->putFile($path, 'file', $newMetadata);

    $sampleFile = $fileManager->getFile($path);

    $metadata = $fileManager->getMetadata($path);

    $fileManager->remove($path);

}catch (UnableToRetrieveMetadata | UnableToGetFile | UnableToPutFile | UnableToDeleteFile $exception){
    print_r($exception->getMessage());
}
