<?php

namespace FileManager\Contracts;

use FileManager\Exceptions\UnableToCheckFolderExistence;
use FileManager\Exceptions\UnableToGetFolderContents;

interface FolderReaderInterface
{
    public const RECURSIVE = true;

    /**
     * Returns the content of the folder
     *
     * @param string $path
     * @param bool $recursive
     * @return array
     *
     * @throws UnableToGetFolderContents
     */
    public function folderContents(string $path, bool $recursive = self::RECURSIVE ): array;


    /**
     * @param string $path
     * @return bool
     *
     * @throws UnableToCheckFolderExistence
     */
    public function folderExists(string $path): bool;
}