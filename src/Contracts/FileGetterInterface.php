<?php

namespace FileManager\Contracts;

use FileManager\Exceptions\UnableToCheckFileExistence;
use FileManager\Exceptions\UnableToGetFile;
use FileManager\Exceptions\UnableToRetrieveMetadata;
use FileManager\Models\File\Metadata;

interface FileGetterInterface
{
    /**
     * Returns the content of the file
     *
     * @param string $path
     * @return string
     *
     * @throws UnableToGetFile
     */
    public function getFile(string $path): string;


    /**
     * Check the file exists or not
     *
     * @param string $path
     * @return bool
     *
     * @throws UnableToCheckFileExistence
     */
    public function fileExists(string $path): bool;


    /**
     * @param string $path
     * @return Metadata
     *
     * @throws UnableToRetrieveMetadata
     */
    public function getMetadata(string $path): Metadata;
}