<?php

namespace FileManager\Contracts;

use FileManager\Exceptions\UnableToCheckFileExistence;
use FileManager\Exceptions\UnableToCheckFolderExistence;
use FileManager\Exceptions\UnableToCreateFolder;
use FileManager\Exceptions\UnableToDeleteFile;
use FileManager\Exceptions\UnableToDeleteFolder;
use FileManager\Exceptions\UnableToGetFile;
use FileManager\Exceptions\UnableToGetFolderContents;
use FileManager\Exceptions\UnableToPutFile;
use FileManager\Exceptions\UnableToRetrieveMetadata;
use FileManager\Models\File\Metadata;

interface FileAdapter
{
    /**
     * Returns the content of the file
     *
     * @param string $path
     * @return string
     *
     * @throws UnableToGetFile
     */
    public function getFile(string $path): string;


    /**
     * Check the file exists or not
     *
     * @param string $path
     * @return bool
     *
     * @throws UnableToCheckFileExistence
     */
    public function fileExists(string $path): bool;


    /**
     * @param string $path
     * @return Metadata
     *
     * @throws UnableToRetrieveMetadata
     */
    public function getMetaData(string $path): Metadata;


    /**
     * Sets the content to the file
     *
     * @param string $path
     * @param string $file
     * @param Metadata $metadata
     *
     * @throws UnableToPutFile
     */
    public function putFile(string $path, string $file, Metadata $metadata): void;


    /**
     * Removes the file
     *
     * @param string $path
     * @return bool
     *
     * @throws UnableToDeleteFile
     */
    public function remove(string $path): bool;


    public const RECURSIVE = true;

    /**
     * Returns the content of the folder
     *
     * @param string $path
     * @param bool $recursive
     * @return array
     *
     * @throws UnableToGetFolderContents
     */
    public function folderContents(string $path, bool $recursive = self::RECURSIVE ): array;


    /**
     * @param string $path
     * @return bool
     *
     * @throws UnableToCheckFolderExistence
     */
    public function folderExists(string $path): bool;


    /**
     * Create the new folder
     *
     * @param string $path
     * @return void
     *
     * @throws UnableToCreateFolder
     */
    public function createFolder(string $path): void;


    /**
     * Removes the folder
     *
     * @param string $path
     * @return bool
     *
     * @throws UnableToDeleteFolder
     */
    public function removeFolder(string $path): bool;
}