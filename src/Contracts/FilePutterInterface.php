<?php

namespace FileManager\Contracts;

use FileManager\Exceptions\UnableToDeleteFile;
use FileManager\Exceptions\UnableToPutFile;
use FileManager\Models\File\Metadata;

interface FilePutterInterface
{
    /**
     * Sets the content to the file
     *
     * @param string $path
     * @param string $file
     * @param Metadata $metadata
     *
     * @throws UnableToPutFile
     */
    public function putFile(string $path, string $file, Metadata $metadata): void;


    /**
     * Removes the file
     *
     * @param string $path
     * @return bool
     *
     * @throws UnableToDeleteFile
     */
    public function remove(string $path): bool;

}