<?php

namespace FileManager\Contracts;

interface FolderInterface extends FolderPutterInterface, FolderReaderInterface
{

}