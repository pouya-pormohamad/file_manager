<?php

namespace FileManager\Contracts;

use FileManager\Exceptions\UnableToCreateFolder;
use FileManager\Exceptions\UnableToDeleteFolder;

interface FolderPutterInterface
{
    /**
     * Create the new folder
     *
     * @param string $path
     * @return void
     *
     * @throws UnableToCreateFolder
     */
    public function createFolder(string $path): void;


    /**
     * Removes the folder
     *
     * @param string $path
     * @return bool
     *
     * @throws UnableToDeleteFolder
     */
    public function removeFolder(string $path): bool;

}