<?php

namespace FileManager\Exceptions;

use Exception;
use Throwable;

class UnableToGetFolderContents extends Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}