<?php

namespace FileManager\Models\Driver;

use FileManager\Models\File\Metadata;

class ImagineThisIsAmazonS3Drive
{
    public function read(string $name): string
    {

    }

    public function upload(string $name, string $file, Metadata $metadata = NULL): void
    {

    }

    public function has(string $name): bool
    {

    }

    public function delete(string $name): bool
    {

    }

    public function getMetadata(string $name): Metadata
    {

    }

    public function deleteDirectory(string $name): bool
    {

    }

    public function createDirectory(string $name): bool
    {

    }

    public function listContents(string $directoryName, $recursive = false): array
    {

    }

    public function hasDirectory(string $name): bool
    {

    }
}