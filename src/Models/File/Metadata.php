<?php

namespace FileManager\Models\File;

class Metadata
{
    /**
     * @var array
     */
    protected $metadata = [];

    public function __construct(array $metadata = [])
    {
        $this->metadata = $metadata;
    }

    /**
     * Returns the metadata of the file
     */
    public function getMetadata($key, $default = null)
    {
        if ( ! array_key_exists($key, $this->metadata)) {
            return $default;
        }

        return $this->metadata[$key];
    }

    /**
     * Sets the metadata of the file
     *
     * @param array $data
     * @return Metadata
     */
    public function putMetadata($key, $value)
    {
        $this->metadata[$key] = $value;

        return $this;
    }
}