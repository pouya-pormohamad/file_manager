<?php

namespace FileManager\Models\File;

use FileManager\Contracts\FileAdapter;
use FileManager\Contracts\WorkWithFile;

class File implements WorkWithFile
{
    private FileAdapter $adapter;

    /**
     * @param FileAdapter $adapter
     */
    public function __construct(FileAdapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * @inheritDoc
     */
    public function getFile(string $path): string
    {
        return $this->adapter->getFile($path);
    }

    /**
     * @inheritDoc
     */
    public function fileExists(string $path): bool
    {
        return $this->adapter->fileExists($path);
    }

    /**
     * @inheritDoc
     */
    public function getMetadata(string $path): Metadata
    {
        return $this->adapter->getMetaData($path);
    }

    /**
     * @inheritDoc
     */
    public function putFile(string $path, string $file, Metadata $metadata): void
    {
        $this->adapter->putFile($path, $file, $metadata);
    }

    /**
     * @inheritDoc
     */
    public function remove(string $path): bool
    {
        return $this->adapter->remove($path);
    }

    /**
     * @inheritDoc
     */
    public function createFolder(string $path): void
    {
        $this->adapter->createFolder($path);
    }

    /**
     * @inheritDoc
     */
    public function removeFolder(string $path): bool
    {
        return $this->adapter->removeFolder($path);
    }

    /**
     * @inheritDoc
     */
    public function folderContents(string $path, bool $recursive = self::RECURSIVE): array
    {
        return $this->adapter->folderContents($path, $recursive);
    }

    /**
     * @inheritDoc
     */
    public function folderExists(string $path): bool
    {
        return $this->adapter->folderExists($path);
    }
}