<?php

namespace FileManager\Models\Adapter;

use FileManager\Contracts\FileAdapter;
use FileManager\Models\Driver\ImagineThisIsAmazonS3Drive;

class AmazonS3Adapter implements FileAdapter
{
    private ImagineThisIsAmazonS3Drive $amazonS3;

    /**
     * @param ImagineThisIsAmazonS3Drive $AmazonS3
     */
    public function __construct(ImagineThisIsAmazonS3Drive $AmazonS3)
    {
        $this->amazonS3 = $AmazonS3;
    }

    public function getFile(string $path): string
    {
       return $this->amazonS3->read($path);
    }

    public function fileExists(string $path): bool
    {
        return $this->amazonS3->has($path);
    }

    public function getMetaData(string $path): Metadata
    {
        return $this->amazonS3->getMetadata($path);
    }

    public function putFile(string $path, string $file, Metadata $metadata): void
    {
        $this->amazonS3->upload($path, $file, $metadata);
    }

    public function removeFolder(string $path): bool
    {
        return $this->amazonS3->deleteDirectory($path);
    }

    public function createFolder(string $path): void
    {
        $this->amazonS3->createDirectory($path);
    }

    public function folderContents(string $path, bool $recursive = self::RECURSIVE): array
    {
        return $this->amazonS3->listContents($path, $recursive);
    }

    public function folderExists(string $path): bool
    {
        return $this->amazonS3->hasDirectory($path);
    }

    public function remove(string $path): bool
    {
        return $this->amazonS3->delete($path);
    }
}