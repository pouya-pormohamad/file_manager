<?php

namespace FileManager\Models\Adapter;

use FileManager\Contracts\FileAdapter;
use FileManager\Models\Driver\DatabaseDrive;
use FileManager\Models\File\Metadata;

class DatabaseAdapter implements FileAdapter
{
    private DatabaseDrive $database;

    /**
     * @param DatabaseDrive $database
     */
    public function __construct(DatabaseDrive $database)
    {
        $this->database = $database;
    }

    public function getFile(string $path): string
    {
       return $this->database->read($path);
    }

    public function fileExists(string $path): bool
    {
        return $this->database->has($path);
    }

    public function getMetaData(string $path): Metadata
    {
        return $this->database->getMetadata($path);
    }

    public function putFile(string $path, string $file, Metadata $metadata): void
    {
        $this->database->upload($path, $file, $metadata);
    }

    public function removeFolder(string $path): bool
    {
        return $this->database->deleteDirectory($path);
    }

    public function createFolder(string $path): void
    {
        $this->database->createDirectory($path);
    }

    public function folderContents(string $path, bool $recursive = self::RECURSIVE): array
    {
        return $this->database->listContents($path, $recursive);
    }

    public function folderExists(string $path): bool
    {
        return $this->database->hasDirectory($path);
    }

    public function remove(string $path): bool
    {
        return $this->database->delete($path);
    }
}